from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
from amazoncaptcha import AmazonCaptcha
import requests
import os

# import numpy as np
# import cv2
# from pytesseract import image_to_string

app = FastAPI()
token = 'ERTASDFAEWRDFGADFGADFGADFG'


class Item(BaseModel):
    image_url: str
    content: str


@app.get("/")
def read_root():
    return {"success": "true", "response": "Hello World!"}


@app.post("/link/")
def solveFromLink(image_url: Optional[str] = None):
    if image_url != "":
        captcha = AmazonCaptcha.fromlink(image_url)
        txt = captcha.solve()
        if txt != "":
            return {"success": "true", "response": {"image_url": image_url, "captcha_text": txt}}
        else:
            return {"success": "false", "response": {"image_url": image_url, "captcha_text": txt}}
    else:
        return {"response": "Empty image URL"}


@app.post("/image/")
def solveFromImage(image_url: Optional[str] = None, proxy_url: Optional[str] = None):
    if image_url != "":
        image_path = os.getcwd()+"/tmp/captcha.jpeg"
        # os.chmod(image_path, 0o777)
        session = requests.Session()
        session.proxies = {
            proxy_url,
        }
        img_data = session.get(image_url, timeout=10).content
        f = open(image_path, 'wb')
        f.write(img_data)
        captcha = AmazonCaptcha(image_path)
        txt = captcha.solve()
        f.close()
        if txt != "":
            return {"success": "true", "response": {"image_url": image_url, "captcha_text": txt}}
        else:
            return {"success": "false", "response": {"image_url": image_url, "captcha_text": txt}}
    else:
        return {"response": "Empty image URL"}


@app.post("/content/")
def solveFromContent(content: Optional[str] = None):
    if content != "":
        # return {"data" : content}
        image_path = os.getcwd()+"/tmp/captcha.jpeg"
        # os.chmod(image_path, 0o777)
        # session = requests.Session()
        # img_data = session.get(content, timeout=10).content
        #  img_data = urllib.parse.unquote(content)
        img_data = bytes(content, 'utf-8')
        # return {"data" : img_data}
        f = open(image_path, 'wb')
        f.write(img_data)
        captcha = AmazonCaptcha(image_path)
        txt = captcha.solve()
        f.close()
        if txt != "":
            return {"success": "true", "response": {"captcha_text": txt}}
        else:
            return {"success": "false", "response": {"captcha_text": txt}}
    else:
        return {"response": "Empty image URL"}

# @app.post("/ml/")
# def solveCaptchaViaAImodel(image_url: Optional[str] = None):
#     print('Solving using OpenCV and Pytesseract.....')
#     img = cv2.imread("captcha2.jpeg")
#     gry = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     gry = cv2.copyMakeBorder(gry, 8, 8, 8, 8, cv2.BORDER_REPLICATE)
#     # (h, w) = gry.shape[:2]
#     # gry = cv2.resize(gry, (w*2, h*2))
#     # cls = cv2.morphologyEx(gry, cv2.MORPH_CLOSE, None)
#     # thr = cv2.threshold(cls, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
#     thr = cv2.threshold(gry, 127, 255, cv2.THRESH_BINARY_INV, cv2.THRESH_OTSU)[1]
#     # txt = image_to_string(thr)
#     # print(txt)
#     # exit()
#     contours = cv2.findContours(thr.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#     contours = contours[0]
#     letter_image_regions = []
#     for contour in contours:
#         # Get the rectangle that contains the contour
#         (x, y, w, h) = cv2.boundingRect(contour)

#         # Compare the width and height of the contour to detect letters that
#         # are conjoined into one chunk
#         if w / h > 1.25:
#             # This contour is too wide to be a single letter!
#             # Split it in half into two letter regions!
#             half_width = int(w / 2)
#             letter_image_regions.append((x, y, half_width, h))
#             letter_image_regions.append((x + half_width, y, half_width, h))
#         else:
#             # This is a normal letter by itself
#             letter_image_regions.append((x, y, w, h))
#     if len(letter_image_regions) != 6:
#         print('Extract Letter Not Equal to Six Digit')
#         exit()
#     else:
#         letter_image_regions = sorted(letter_image_regions, key=lambda x: x[0])
#         output = cv2.merge([gry] * 3)
#         predictions = []
#         # Save out each letter as a single image
#         for letter_bounding_box in letter_image_regions:
#             # Grab the coordinates of the letter in the image
#             x, y, w, h = letter_bounding_box

#             # Extract the letter from the original image with a 2-pixel margin around the edge
#             letter_image = gry[y - 2:y + h + 2, x - 2:x + w + 2]

#             letter_image = cv2.resize(letter_image, (30,30))

#             # Turn the single image into a 4d list of images
#             letter_image = np.expand_dims(letter_image, axis=2)
#             letter_image = np.expand_dims(letter_image, axis=0)

#             # making prediction
#             pred = model.predict(letter_image)

#             # Convert the one-hot-encoded prediction back to a normal letter
#             letter = lb.inverse_transform(pred)[0]
#             predictions.append(letter)

#             # draw the prediction on the output image
#             cv2.rectangle(output, (x - 2, y - 2), (x + w + 4, y + h + 4), (0, 255, 0), 1)
#             cv2.putText(output, letter, (x - 5, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)

#         # Print the captcha's text
#         captcha_text = "".join(predictions)
#         print("CAPTCHA text is: {}".format(captcha_text))

#     return {'msg': 'Work In progress'}