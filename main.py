from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
# from requests.auth import HTTPProxyAuth
from amazoncaptcha import AmazonCaptcha
import base64
import uvicorn
import random
import string
import requests
import os, time

app = FastAPI()
token = 'ERTASDFAEWRDFGADFGADFGADFG'

class Item(BaseModel):
    image_url: str

@app.get("/")
def read_root():
    return {"success": "true", "response": "Hello World!"}


@app.post("/content/")
def solveFromContent(content):
    if content != "":
        content = base64.b64decode(content)
        random = random_string(12)
        image_path = os.getcwd()+"/tmp/captcha_"+random+".jpg"
        # os.chmod(image_path, 0o777)
        f = open(image_path, 'wb+')
        f.write(content)
        f.close()
        captcha = AmazonCaptcha(image_path)
        txt = captcha.solve()
        os.unlink(image_path)  # delete image
        time.sleep(1)
        if txt != "":
            return {"success": "true", "response": {"captcha_text": txt}}
        else:
            return {"success": "false", "response": {"captcha_text": txt}}
    else:
        return {"response": "Empty Content"}


@app.post("/link/")
def solveFromLink(image_url: Optional[str] = None):
    if image_url != "":
        captcha = AmazonCaptcha.fromlink(image_url)
        txt = captcha.solve()
        if txt != "":
            return {"success": "true", "response": {"image_url": image_url, "captcha_text": txt}}
        else:
            return {"success": "false", "response": {"image_url": image_url, "captcha_text": txt}}
    else:
        return {"response": "Empty image URL"}


@app.post("/image/")
def solveFromImage(image_url: Optional[str] = None, proxy_url: Optional[str] = None):
    if image_url != "":
        session = requests.Session()
        # username = 'pvortex'
        # password = 'Dn4wFvV9'
        # session.proxies = {
        #     proxy_url
        # }
        # session.auth = {
        #     HTTPProxyAuth(username, password)
        # }
        img_data = session.get(image_url, timeout=10).content
        random = random_string(12)
        image_path = os.getcwd()+"/tmp/captcha_"+random+".jpg"
        # os.chmod(image_path, 0o777)
        f = open(image_path, 'wb+')
        f.write(img_data)
        f.close()
        captcha = AmazonCaptcha(image_path)
        txt = captcha.solve()
        os.unlink(image_path)  # delete image
        if txt != "":
            return {"success": "true", "response": {"image_url": image_url, "captcha_text": txt}}
        else:
            return {"success": "false", "response": {"image_url": image_url, "captcha_text": txt}}
    else:
        return {"response": "Empty image URL"}


def random_string(length):
    letters = string.ascii_lowercase
    result = ''.join((random.choice(letters)) for x in range(length))
    return result


if __name__ == "__main__":
    uvicorn.run(app, host='127.0.0.1', port=8000, debug=True)
